package main

import (
	"embed"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path"
	"strings"
	"syscall"
	"text/template"
)

//go:embed tmpl/*
var Templates embed.FS

func GetDataPath() (string, error) {
	str, res := os.LookupEnv("BORZOI_HOME")

	if res != false {
		return path.Clean(str), nil
	} else {
		str, res = os.LookupEnv("XDG_DATA_HOME")
		if res != false {
			return path.Join(str, "borzoi"), nil
		} else {
			home, err := os.UserHomeDir()
			if err != nil {
				return "", err
			}
			return path.Join(home, ".local/share/borzoi"), nil
		}
	}
}

type Domain interface {
	GetName() string
	GetConfig(path string) (string, error)
	GetHttpdConfig() (string, error)
	GetAcmeConfig() (string, error)
	DoesAcme() bool
	NextStep()
}

type Certificate struct {
	Certificate, Key, Fullchain string
}

type PlainDomain struct {
	Name, Root string
	Redirect   bool
}

func (d PlainDomain) GetName() string { return d.Name }
func (d PlainDomain) GetConfig(path string) (string, error) {
	var str strings.Builder
	tmplFile, err := Templates.ReadFile("tmpl/" + path)
	if err != nil {
		return "", err
	}
	tmpl, err := template.New(path).Parse(string(tmplFile))
	if err != nil {
		return "", err
	}
	err = tmpl.Execute(&str, d)
	if err != nil {
		return "", err
	}
	return str.String(), nil
}
func (d PlainDomain) GetHttpdConfig() (string, error) {
	return d.GetConfig("httpd.conf")
}
func (d PlainDomain) GetAcmeConfig() (string, error) {
	return "", nil
}
func (d PlainDomain) NextStep() {}
func (d PlainDomain) DoesAcme() bool { return false }

type AcmeDomain struct {
	Name, Root string
	Redirect   bool
	TlsCert    Certificate
	DoAcme     bool
	AltNames   []string
	_Setup     bool
}

func (d AcmeDomain) GetName() string { return d.Name }
func (d AcmeDomain) GetConfig(path string) (string, error) {
	var str strings.Builder

	tmplFile, err := Templates.ReadFile("tmpl/acme/" + path)
	if err != nil {
		return "", err
	}
	tmpl, err := template.New(path).Parse(string(tmplFile))
	if err != nil {
		return "", err
	}
	err = tmpl.Execute(&str, d)
	if err != nil {
		return "", err
	}
	return str.String(), nil
}
func (d AcmeDomain) GetHttpdConfig() (string, error) {
	if !d._Setup {
		return d.GetConfig("httpd-step1.conf")
	} else {
		return d.GetConfig("httpd-step2.conf")
	}
}
func (d AcmeDomain) GetAcmeConfig() (string, error) {
	if d.DoAcme {
		return d.GetConfig("acme-client.conf")
	} else {
		return "", nil
	}
}
func (d *AcmeDomain) NextStep() {
	d._Setup = true
}
func (d AcmeDomain) DoesAcme() bool { return d.DoAcme }


func SetupAll(domains []Domain, dry bool) error {
	var (
		httpd  strings.Builder
		httpd2 strings.Builder
		acme   strings.Builder
		acmePath string
		httpdPath string
	)

	/* write acme-client.conf prelude */
	acmePrelude, err := Templates.ReadFile("tmpl/acme/acme-client-prelude.conf")
	if err != nil {
		return err
	}
	acme.WriteString(string(acmePrelude))

	/* write complete httpd.conf and acme-client.conf and do pertinent setup */
	for _, d := range domains {
		hcfg, err := d.GetHttpdConfig()
		if err != nil {
			return err
		}
		acfg, err := d.GetAcmeConfig()
		if err != nil {
			return err
		}
		httpd.WriteString(hcfg)
		acme.WriteString(acfg)
	}

	dataPath, err := GetDataPath()
	if err != nil {
		return err
	}
	httpdPath = path.Join(dataPath, "httpd.conf")
	acmePath = path.Join(dataPath, "acme-client.conf")

	if !dry {
		log.Println(":: create directory", dataPath)
		err = os.MkdirAll(dataPath, 0755)
		if err != nil {
			return err
		}
		log.Println(":: create temp httpd.conf")
		fd1, err := os.Create(httpdPath)
		defer fd1.Close()
		_, err = fd1.WriteString(httpd.String())
		if err != nil {
			return err
		}

		log.Println(":: create temp acme-client.conf")
		fd2, err := os.Create(acmePath)
		defer fd2.Close()
		_, err = fd2.WriteString(acme.String())
		if err != nil {
			return err
		}

		/* interact with programs */
		log.Println(":: setup httpd.conf")
		log.Println("cmd: mv", httpdPath, "to /etc/httpd.conf")
		err = os.Rename(httpdPath, "/etc/httpd.conf")
		if err != nil {
			return err
		}
		log.Println(":: setup acme-client.conf")
		log.Println("cmd: mv", acmePath, "to /etc/acme-client.conf")
		err = os.Rename(acmePath, "/etc/acme-client.conf")
		if err != nil {
			return err
		}

		log.Println(":: enable and start httpd")
		log.Println("cmd: rcctl enable httpd")
		cmd := exec.Command("rcctl", "enable", "httpd")
		err = cmd.Run()
		if err != nil {
			return err
		}
		log.Println("cmd: rcctl restart httpd")
		cmd = exec.Command("rcctl", "restart", "httpd")
		err = cmd.Run()
		if err != nil {
			return err
		}
		log.Println(":: get certificates")
		for _, d := range domains {
			if d.DoesAcme() {
				log.Println("cmd: acme-client -v", d.GetName())
				cmd = exec.Command("acme-client", "-v", d.GetName())
				err = cmd.Run()
				if err != nil {
					if exiterr, ok := err.(*exec.ExitError); ok {
						if status, ok := exiterr.Sys().(syscall.WaitStatus); ok {
							if status.ExitStatus() == 2 {
								log.Println("!! certificate already valid")
							}
						}
					} else {
						return err
					}
				}
			}
		}
	} else {
		log.Println(":: create directory", dataPath)
		log.Println(":: create temp httpd.conf")
		log.Println(":: create temp acme-client.conf")
		log.Println(":: setup httpd.conf")
		log.Println("cmd: mv", httpdPath, "to /etc/httpd.conf")
		log.Println(":: setup acme-client.conf")
		log.Println("cmd: mv", acmePath, "to /etc/acme-client.conf")
		log.Println(":: enable and start httpd")
		log.Println("cmd: rcctl enable httpd")
		log.Println("cmd: rcctl restart httpd")
		log.Println(":: get certificates")
		for _, d := range domains {
			if d.DoesAcme() {
				log.Println("cmd: acme-client -v", d.GetName())
			}
		}
	}

	for _, d := range domains {
		d.NextStep()
	}

	/* write new httpd.conf */
	for _, d := range domains {
		hcfg, err := d.GetHttpdConfig()
		if err != nil {
			return err
		}
		httpd2.WriteString(hcfg)
	}

	if !dry {
		log.Println(":: write new temp httpd.conf")
		fd, err := os.Create(httpdPath)
		defer fd.Close()
		_, err = fd.WriteString(httpd2.String())
		if err != nil {
			return err
		}

		log.Println(":: reload httpd")
		log.Println("cmd: mv", httpdPath, "/etc/httpd.conf")
		err = os.Rename(httpdPath, "/etc/httpd.conf")
		if err != nil {
			return err
		}
		log.Println("cmd: rcctl reload httpd")
		cmd := exec.Command("rcctl", "reload", "httpd")
		err = cmd.Run()
		if err != nil {
			return err
		}
	} else {
		log.Println(":: remove old temp httpd.conf")
		log.Println(":: write new temp httpd.conf")
		log.Println(":: reload httpd")
		log.Println("cmd: mv", httpdPath, "/etc/httpd.conf")
		log.Println("cmd: rcctl reload httpd")
	}


	return nil
}

type DomainConfig struct {
	Acme  *AcmeDomain
	Plain *PlainDomain
}
func GetDomains(path string) ([]Domain, error) {
	var domains []Domain
	var dmcfg []DomainConfig
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(data, &dmcfg)
	if err != nil {
		return nil, err
	}
	for _, dc := range dmcfg {
		if dc.Acme != nil {
			domains = append(domains, dc.Acme)
		} else {
			domains = append(domains, dc.Plain)
		}
	}
	return domains, nil
}

func main() {
	dryRun := flag.Bool("dry", false, "")
	cfgPath := flag.String("cfg", "./borzoi.json", "")
	flag.Parse()

	domains, err := GetDomains(*cfgPath)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	err = SetupAll(domains, *dryRun)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
}
