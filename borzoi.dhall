let Cert   = { Certificate: Text, Key: Text, Fullchain: Text }
let Acme   = { Name: Text, Root: Text, Redirect: Bool, TlsCert: Optional Cert, DoAcme: Bool, AltNames: Optional (List Text) }
let Plain  = { Name: Text, Root: Text, Redirect: Bool }
let Domain = < Acme : { Acme: Acme } | Plain : { Plain: Plain } >
let getCert = \(x : Text) -> {
    Certificate = "/etc/ssl/${x}.crt",
    Key = "/etc/ssl/private/${x}.key",
    Fullchain = "/etc/ssl/${x}.pem"
} : Cert
let defaultAcme = \(x : Text) -> \(a : Optional (List Text)) -> Domain.Acme {
    Acme = {
        Name = x,
        Root = "/htdocs/${x}",
        Redirect = False,
        TlsCert = Some (getCert x),
        DoAcme = True,
        AltNames = a
    }
}
let defaultAcmeRedirect = \(src : Text) -> \(trg : Text) -> Domain.Acme {
    Acme = {
        Name = src,
        Root = "https://${trg}$REQUEST_URI",
        Redirect = True,
        TlsCert = Some (getCert trg),
        DoAcme = False,
        AltNames = None (List Text)
    }
}

in [
    defaultAcme "sergal.lol" (Some [ "www.sergal.lol" ]),
    defaultAcmeRedirect "www.sergal.lol" "sergal.lol",

    defaultAcme "hyena.pictures" (None (List Text)),
    defaultAcme "piss.institute" (None (List Text)),
    defaultAcme "pissk.ink" (None (List Text)),
    defaultAcme "sergal.institute" (None (List Text)),
    defaultAcme "swirly.world" (None (List Text)),
    defaultAcme "muon.red" (None (List Text))
] : List Domain